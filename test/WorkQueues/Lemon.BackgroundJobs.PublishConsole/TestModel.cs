namespace Lemon.BackgroundJobs.PublishConsole;

public class TestModel
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
}