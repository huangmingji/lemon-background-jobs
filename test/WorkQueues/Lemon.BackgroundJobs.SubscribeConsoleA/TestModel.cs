namespace Lemon.BackgroundJobs.SubscribeConsoleA;

public class TestModel
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
}