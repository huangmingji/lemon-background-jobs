using Lemon.BackgroundJobs.RabbitMQ.DelayQueues;
using Lemon.Common.Extend;
using Microsoft.Extensions.DependencyInjection;

namespace Lemon.BackgroundJobs.SubscribeConsole;

public class TestJob : DelayQueuesJob<TestModel>
{
    public TestJob(IServiceCollection serviceCollection) 
        : base(serviceCollection, "DelayTestJob")
    {
    }

    protected override void Execute(TestModel args)
    {
        Console.WriteLine(args.SerializeObject());
    }
}