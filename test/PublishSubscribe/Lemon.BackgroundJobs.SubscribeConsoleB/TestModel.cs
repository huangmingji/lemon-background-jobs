namespace Lemon.BackgroundJobs.SubscribeConsoleB;

public class TestModel
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
}