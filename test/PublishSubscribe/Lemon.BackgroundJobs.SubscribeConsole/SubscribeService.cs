using Lemon.BackgroundJobs.RabbitMQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Lemon.BackgroundJobs.SubscribeConsole;

public class SubscribeService: IHostedService
{
    private readonly IHostApplicationLifetime _hostApplicationLifetime;

    public SubscribeService(IHostApplicationLifetime hostApplicationLifetime)
    {
        _hostApplicationLifetime = hostApplicationLifetime;
    }
        
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables();

        IConfiguration configuration = builder.Build();

        var services = new ServiceCollection();
        services.AddTransient<IServiceCollection>(x => services);
        services.AddLogging();
        services.AddSingleton<IConfiguration>(configuration);
        services.UseRabbitMQ();
        
        Console.WriteLine("按任意键退出");
        Console.ReadKey();
        
        _hostApplicationLifetime.StopApplication();
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}