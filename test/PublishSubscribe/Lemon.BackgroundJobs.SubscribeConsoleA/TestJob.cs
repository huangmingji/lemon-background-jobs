using Lemon.BackgroundJobs.RabbitMQ.PublishSubscribe;
using Lemon.Common.Extend;
using Microsoft.Extensions.DependencyInjection;

namespace Lemon.BackgroundJobs.SubscribeConsoleA;

public class TestJob : PublishSubscribeJob<TestModel>
{
    public TestJob(IServiceCollection serviceCollection) 
        : base(serviceCollection, nameof(TestJob))
    {
    }

    protected override void Execute(TestModel args)
    {
        Console.WriteLine(args.SerializeObject());
    }
}