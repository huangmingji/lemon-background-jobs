namespace Lemon.BackgroundJobs.Abstractions.WorkQueues;

public interface IWorkQueues
{
    void Publish<T>(T msg, string queue) where T : class, new();

    void Subscribe<T>(string queue, Action<T> handler) where T : class, new();
}