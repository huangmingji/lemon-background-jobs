namespace Lemon.BackgroundJobs.Abstractions.WorkQueues;

public interface IWorkQueuesJob<in T> : IBackgroundJob where T : class, new()
{
}