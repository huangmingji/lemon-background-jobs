namespace Lemon.BackgroundJobs.Abstractions.PublishSubscribe;

public interface IPublishSubscribeJob<in T> : IBackgroundJob where T : class, new()
{
}