namespace Lemon.BackgroundJobs.Abstractions.PublishSubscribe;

public interface IPublishSubscribe
{
    void Publish<T>(T msg, string exchange) where T : class, new();

    void Subscribe<T>(string exchange, Action<T> handler) where T : class, new();
}