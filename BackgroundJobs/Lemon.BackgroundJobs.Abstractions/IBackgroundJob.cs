namespace Lemon.BackgroundJobs.Abstractions;

public interface IBackgroundJob
{
    void Receive();
}
