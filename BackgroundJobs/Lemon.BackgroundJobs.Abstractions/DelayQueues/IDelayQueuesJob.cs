namespace Lemon.BackgroundJobs.Abstractions.DelayQueues;

public interface IDelayQueuesJob<in T> : IBackgroundJob where T : class, new()
{
}