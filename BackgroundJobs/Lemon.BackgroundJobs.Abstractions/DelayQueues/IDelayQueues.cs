namespace Lemon.BackgroundJobs.Abstractions.DelayQueues;

public interface IDelayQueues
{
    void Publish<T>(T msg, string queue, int delay) where T : class, new();

    void Subscribe<T>(string queue, Action<T> handler) where T : class, new();
}