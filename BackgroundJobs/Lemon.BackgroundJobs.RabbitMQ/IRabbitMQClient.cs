using RabbitMQ.Client;

namespace Lemon.BackgroundJobs.RabbitMQ;

public interface IRabbitMQClient
{
    
    IConnection GetConnection();

    IConnection CreateNewConnection();
}