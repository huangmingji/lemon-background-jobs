using Lemon.BackgroundJobs.Abstractions.DelayQueues;
using Microsoft.Extensions.DependencyInjection;

namespace Lemon.BackgroundJobs.RabbitMQ.DelayQueues;

public abstract class DelayQueuesJob<T> : IDelayQueuesJob<T> where T : class, new()
{
    private readonly string _queue;
    private readonly IDelayQueues? _delayQueues;

    protected DelayQueuesJob(IServiceCollection serviceCollection, string queue)
    {
        _queue = queue;
        _delayQueues = serviceCollection.BuildServiceProvider().GetService<IDelayQueues>();
    }

    protected abstract void Execute(T args);
    
    public void Receive()
    {
        if (_delayQueues != null)
        {
            _delayQueues.Subscribe<T>(_queue, this.Execute);
        }
    }
}