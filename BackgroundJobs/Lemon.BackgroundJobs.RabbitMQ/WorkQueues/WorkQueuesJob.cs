using Lemon.BackgroundJobs.Abstractions;
using Lemon.BackgroundJobs.Abstractions.WorkQueues;
using Microsoft.Extensions.DependencyInjection;

namespace Lemon.BackgroundJobs.RabbitMQ.WorkQueues;

public abstract class WorkQueuesJob<T> : IWorkQueuesJob<T> where T : class, new()
{
    private readonly string _queue;
    private readonly IWorkQueues? _workQueues;

    protected WorkQueuesJob(IServiceCollection serviceCollection, string queue)
    {
        _queue = queue;
        _workQueues = serviceCollection.BuildServiceProvider().GetService<IWorkQueues>();
    }

    protected abstract void Execute(T args);
    
    public void Receive()
    {
        if (_workQueues != null)
        {
            _workQueues.Subscribe<T>(_queue, this.Execute);
        }
    }
}